/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.siriphonpha.selllproductintheshopjieb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sirip
 */
public class User implements Serializable {

    private static ArrayList<User> userList = null;
    private static User currentUser = null;
    private static User superAdmin = new User("test", "1");
    

    public static boolean addUser(User user) {
        userList.add(user);
//        save();
        return true;
    }

    //delect (D)
    public static boolean delUser(User user) {
        userList.remove(user);
//        save();
        return true;
    }

    public static boolean delUser(int index) {
        userList.remove(index);
//        save();
        return true;
    }

    // read (R)
    public static ArrayList<User> getUsers() {
        return userList;
    }

    //update (U)
    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
//        save();
        return true;
    }

    public static User getUser(int index) {
        return userList.get(index);
    }

    public static User login(String userName, String password) {
        if (userName.equals("super") && userName.equals("super")) {
            return superAdmin;
        }
        for (int i = 0; i < userList.size(); i++) {
            User user = userList.get(i);
            if (user.getUserName().equals(userName) && user.getPassword().equals(password)) {
                currentUser = user;
                return user;
            }
        }
        return null;
    }

    public static boolean isLogin() {
        return currentUser != null;
    }

    public static User getCurrentUser() {
        return currentUser;
    }

    public static void logout() {
        currentUser = null;
    }

    private String userName;
    private String password;

    public User(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            file = new File("jieb.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("jieb.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String toString() {
        return "userName = " + userName + ",  password = " + password;
    }
}
