/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.siriphonpha.selllproductintheshopjieb;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author sirip
 */
public class Customer {

    static Collection getCustomer() {
        return customerlist;
    }

    private String id;
    private String name;
    private String brand;
    private String price;
    private String amount;

    private static ArrayList<Customer> customerlist = null;

    static {
        customerlist = new ArrayList<>();
        customerlist.add(new Customer("1", "Rice", "easy", "15.0", "1"));
        customerlist.add(new Customer("2", "Sausage", "Ceepee", "35.0", "1"));
        customerlist.add(new Customer("3", "Snack", "ArhanYodkhun", "10.5", "2"));

    }

    public String getPrice() {
        return price;
    }

    static Customer getCustomer(int index) {
        return customerlist.get(index);
    }

    public Customer(String id, String name, String brand, String price, String amount) {
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.amount = amount;
    }

    public static boolean updateCustomer(int index, Customer customer) {
        customerlist.set(index, customer);
        return true;
    }

    public static void setCustomer(ArrayList<Customer> customer) {
        Customer.customerlist = customer;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public static void setCustomerlist(ArrayList<Customer> customerlist) {
        Customer.customerlist = customerlist;
    }

    public static ArrayList<Customer> getCustomerlist() {
        return customerlist;
    }

//    Customer(String id, String name, String brand, String price, String amount) {
//        this.id =id;
//        name = name;
//        brand = brand;
//        price = price;
//        amount = amount;
//    }

    @Override
    public String toString() {
        return  "   ID: " + id + 
                   " ,   NAME:      " + name + 
                   " ,   BRAND:   " + brand + 
                   " ,   PRICE:   " + price + 
                   ",    AMOUNT:   "+ amount ;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBrand() {
        return brand;
    }

    public String getAmount() {
        return amount;
    }

    public static boolean addCustomer(Customer customer) {
        customerlist.add(customer);
        return true;
    }

    public static boolean delCustomer(Customer customer) {
        customerlist.remove(customer);
        return true;
    }

    public static boolean delProduct(int index) {
        customerlist.remove(index);
        return true;
    }

}
